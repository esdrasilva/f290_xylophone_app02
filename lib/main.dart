// Importação do pacote AudioCache
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.deepOrange),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        // Chamada ao Widget personalizado.
        BotaoXilofone(
          numero: 1,
          cor: Colors.red,
        ),
        BotaoXilofone(
          numero: 2,
          cor: Colors.orange,
        ),
        BotaoXilofone(
          numero: 3,
          cor: Colors.yellow,
        ),

        // TODO: Adicionar os demais botões com os respectivos sons.
      ],
    ));
  }
}

// Widget cusmozidado para facilitar a criação dos botões
class BotaoXilofone extends StatelessWidget {
  // Atributos utilizados como parametros de customização
  final int numero;
  final Color cor;

  const BotaoXilofone({this.numero, this.cor});

  // Metodo que constroi o botão expandido representando as teclas do Xylophone
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
        color: cor,
        onPressed: () {
          // Criacao de objeto player de aúdio
          var player = AudioCache();

          // Execução da nota utilizando StringTemplate ou interpolação de string com base na pasta ASSET
          /*
            /assets
              `-> note1.wav
              `-> note2.wav
          */
          // A Pasta asset foi configurada no arquivo puspech.yaml
          player.play('note$numero.wav');
        },
      ),
    );
  }
}
